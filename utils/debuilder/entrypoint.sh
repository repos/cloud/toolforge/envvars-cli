#!/bin/bash

set -o pipefail
set -o nounset
set -o errexit

restore_user() {
    current_user=$(stat . --format=%u)
    current_group=$(stat . --format=%g)
    find /src -user root -exec chown "${current_user}:${current_group}" {} + 2>/dev/null || true
}

trap restore_user EXIT

export DEBIAN_FRONTEND=noninteractive
cd /src

apt-get update -y
apt-get \
    build-dep \
    --yes \
    --target-release "${DISTRO}"-backports \
    .

debuild -uc -us
dh clean

rm -rf build
mkdir build
mv ../*.deb build/
echo -e "\n\n###############################\nYour packages can be found now under:"
ls ./build/*
