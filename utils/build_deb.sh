#!/bin/bash

set -o pipefail
set -o errexit

show_help() {
    cat <<EOH
Usage: build_deb.sh [OPTIONS] [DISTRO]

Options:
  --no-cache    Build the Docker image without using the cache
  --help, -h    Show this help message and exit

DISTRO:
  Specify the Debian distribution to use (bullseye or bookworm).
  Default is bullseye.
EOH
}

if [[ "${1}" == "--help" || "${1}" == "-h" ]]; then
    show_help
    exit 0
fi

if [[ "${1}" == "--no-cache" ]]; then
    no_cache=(--no-cache)
    shift
else
    no_cache=()
fi

if [[ "${1}" == "bullseye" ]] || [[ "${1}" == "bookworm" ]]; then
    distro="${1}"
    shift
else
    distro="bullseye"
fi

DOCKER="docker"
extra_options=("--volume=$PWD:/src:rw")
if command -v podman >/dev/null; then
    DOCKER="podman"
    extra_options=(
        "--volume=$PWD:/src:rw,z"
        "--userns=keep-id"
    )
fi

$DOCKER build --build-arg DISTRO="${distro}" "utils/debuilder" "${no_cache[@]}" -t "debuilder-${distro}:latest"
# cleanup old packages/build artifacts
rm -rf build .pybuild
# we need root privileges to install packages
$DOCKER run --user 0 --rm "${extra_options[@]}" "debuilder-${distro}:latest"
