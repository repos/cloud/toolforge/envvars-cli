#!/usr/bin/env bash
PROJECT=envvars-cli
BUMP_BRANCH=bump_$PROJECT

set -o errexit
set -o pipefail

show_help() {
    cat <<EOH
Usage: bump_version.sh [OPTIONS] [DISTRO]

Options:
  --no-cache    Build the Docker image without using the cache
  --help, -h    Show this help message and exit

DISTRO:
  Specify the Debian distribution to use (bullseye or bookworm).
  Default is bullseye.
EOH
}

if [[ "${1}" == "--help" || "${1}" == "-h" ]]; then
    show_help
    exit 0
fi

if [[ "${1}" == "--no-cache" ]]; then
    no_cache=(--no-cache)
    shift
else
    no_cache=()
fi

email="$(git config user.email)"
name="$(git config user.name)"
if [[ "${1}" == "bullseye" ]] || [[ "${1}" == "bookworm" ]]; then
    distro="${1}"
    shift
else
    distro="bullseye"
fi

echo "Using DISTRO: ${distro}"

DOCKER="docker"
extra_options=(
        "--volume=$PWD:/src:rw"
)
if command -v podman >/dev/null; then
    DOCKER="podman"
    extra_options=(
        "--userns=keep-id"
        "--volume=$PWD:/src:rw,z"
    )
fi


current_branch="$(git rev-parse --abbrev-ref HEAD)"
if [[ "$current_branch" != "$BUMP_BRANCH" ]]; then
    echo "Switching to version bump branch $BUMP_BRANCH"
    git switch --force-create $BUMP_BRANCH
    git reset --hard "HEAD@{1}"
fi

$DOCKER build --build-arg DISTRO="${distro}" "utils/debuilder" "${no_cache[@]}" -t "debuilder-${distro}:latest"
$DOCKER run \
    --entrypoint /generate_changelog.sh \
    --env "EMAIL=${email}" \
    --env "NAME=${name}" \
    --rm \
    "--user=$UID" \
    "${extra_options[@]}" \
    "debuilder-${distro}:latest" \
    "$@"

new_version="$(head -n1 debian/changelog | grep -o '[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+')"
new_message="$(sed '/^toolforge-/,$!d;/^ *--/q' debian/changelog | awk 'NR > 2 {print prev} {prev=$0}')"
latest_tag="$(
    git ls-remote --refs --tags origin \
    | awk '{print $2}' | sort -V | tail -n 1 | sed -e 's/refs\/tags\///'
)"
bugs="$(
    git log "$latest_tag"..HEAD | grep -o "Bug: T[[:digit:]]*" || :
)"

git commit --signoff --all --message "d/changelog: bump to $new_version

$new_message

$bugs"

git tag "debian/$new_version"
