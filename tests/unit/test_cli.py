#!/usr/bin/env python3
from __future__ import annotations

import json
from typing import Any
from unittest.mock import patch

from click.testing import CliRunner

from toolforge_envvars_cli.cli import toolforge_envvars


def get_fake_envvar_create_response(**kwargs) -> dict[str, Any]:
    envvar = {
        "name": "dummyvar",
        "value": "dummyvalue",
    }
    envvar.update(kwargs)
    return {
        "envvar": envvar,
        "messages": {"error": [], "warning": [], "info": []},
    }


class TestCreate:
    @patch("toolforge_envvars_cli.cli.EnvvarsClient")
    def test_simple(self, envvars_client_mock):
        fake_var_response = get_fake_envvar_create_response()
        envvars_client_mock.from_config().post.return_value = fake_var_response

        result = CliRunner().invoke(cli=toolforge_envvars, args=["create", "MY_ENVVAR", "with some value"])

        assert result.exit_code == 0
        assert fake_var_response["envvar"]["name"] in result.output
        assert fake_var_response["envvar"]["value"] in result.output

    @patch("toolforge_envvars_cli.cli.EnvvarsClient")
    def test_json(self, envvars_client_mock):
        fake_var_response = get_fake_envvar_create_response()
        envvars_client_mock.from_config().post.return_value = fake_var_response

        result = CliRunner().invoke(cli=toolforge_envvars, args=["create", "MY_ENVVAR", "with some value", "--json"])

        assert result.exit_code == 0
        parsed_result = json.loads(result.output)
        assert parsed_result == fake_var_response

    @patch("toolforge_envvars_cli.cli._should_prompt", return_value=True)
    @patch("toolforge_envvars_cli.cli.EnvvarsClient")
    def test_prompt_value_on_tty(self, envvars_client_mock, should_prompt_mock):
        fake_value = "some value"
        fake_var_response = get_fake_envvar_create_response(value=fake_value)
        envvars_client_mock.from_config().post.return_value = fake_var_response

        result = CliRunner().invoke(cli=toolforge_envvars, args=["create", "MY_ENVVAR"], input=f"{fake_value}\n")

        assert result.exit_code == 0
        assert "Enter the value" in result.output
        assert fake_var_response["envvar"]["name"] in result.output
        assert fake_var_response["envvar"]["value"] in result.output
        assert result.output.count(fake_var_response["envvar"]["value"]) == 2

    @patch("toolforge_envvars_cli.cli._should_prompt", return_value=False)
    @patch("toolforge_envvars_cli.cli.EnvvarsClient")
    def test_read_stdin_when_not_on_tty(self, envvars_client_mock, should_prompt_mock):
        fake_value = "some value"
        fake_var_response = get_fake_envvar_create_response(value=fake_value)
        envvars_client_mock.from_config().post.return_value = fake_var_response

        result = CliRunner().invoke(cli=toolforge_envvars, args=["create", "MY_ENVVAR"], input=f"{fake_value}\n")

        assert result.exit_code == 0
        assert "Enter the value" not in result.output
        assert fake_var_response["envvar"]["name"] in result.output
        assert fake_var_response["envvar"]["value"] in result.output
        # Make sure there was no prompt
        assert result.output.count(fake_var_response["envvar"]["value"]) == 1
